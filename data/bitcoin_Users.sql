-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: bitcoin
-- ------------------------------------------------------
-- Server version	5.6.30-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (5,'string','string+6','$2a$10$U6mpi7JPbGNkbhXp2Uv3r.TxhRxQ31V.O36HbyLDL.uQ9rp1udHb6','ducl+6@greenglobal.vn',1,NULL,'9df190f4-1ed2-5b13-9998-626e1f8e1b74'),(6,NULL,NULL,'$2a$10$yqoYzngnSAyOABd32B1IMOlqwckVMdL12I7O0uQdQiSfFK5LrQ4Me','ducl+8@greenglobal.vn',NULL,NULL,'87fe57ed-d8a4-5183-a4b7-c7d32b22c090'),(7,NULL,'ducl+9','$2a$10$QSi0TReYjR2KoVPNviUYJ.3oThpaxM.EDqa2Bjeebofk9ENZFV8ES','ducl@greenglobal.vn',NULL,NULL,'5e8c51b4-870c-5a37-92fd-1eacebc107de'),(8,NULL,'ducl+1','$2a$10$MMVu2moQ4a50372R7bhsGuarsrPur5EA7.RRE1p.6CsmiZ4E5i6/a','ducl+1@greenglobal.vn',NULL,NULL,'e6dc5500-d1ee-5743-974d-46e4f1bfbc1b');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-21 14:31:04
