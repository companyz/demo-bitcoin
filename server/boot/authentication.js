module.exports = function enableAuthentication(server) {
  // enable authentication
  server.use(server.loopback.context());
  server.use(server.loopback.token());
  server.use(function setCurrentUser(req, res, next) {
    if (!req.accessToken) {
      return next();
    }
    server.models.Users.findById(req.accessToken.userId, function (err, user){
      if (err) {
        return next(err);
      }
      if (!user) {
        return next(new Error('No user with this access token was found.'));
      }
      var loopbackContext = server.loopback.getCurrentContext();

      if (loopbackContext) {
        loopbackContext.set('currentUser', user);
      }
      next();
    });
  });

  server.enableAuth();
};
